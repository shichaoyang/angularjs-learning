angular.module('greatApp').directive('postInput', ['$rootScope','Posts', function($rootScope, Posts) {

  return {
    restrict: 'A',
    scope: {},
    controller: function($scope) {
      $scope.addPost = function() {
        Posts.posts.push($scope.newPost);
        $scope.newPost = '';
        $rootScope.$broadcast('post:added');
      };
    },
    template: ["<form class='navbar-form navbar-left'>",
               "<div class='form-group'>",
               "<input ng-model='newPost'  type='text' class='form-control' placeholder='Input Value'>",
               "</div>",
               "<button type='submit' ng-click='addPost()' class='btn btn-default'>ADD</button>",
               "</form>"
              ].join('')
  }

}]);